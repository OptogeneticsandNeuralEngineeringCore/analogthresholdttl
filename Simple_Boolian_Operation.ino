/*
   Simple Boolian Opperation on two digital signals.
   Optogenetics and Neural Engineering Core ONE Core
   University of Colorado
   10/5/16
   See bit.ly/onecore for more information, someday

   Intro
*/


int pauseIn = 10;      // 3i pulse enable in
int thresholdIn = 9;  // 3i pause. ECG thresholded TTL
int pauseOut = 11;      // 3i pulse enable out

int pauseState = LOW;
int thresholdState = LOW; 

void setup() {
  pinMode(pauseIn, INPUT);
  pinMode(thresholdIn, INPUT);
  pinMode(pauseOut, OUTPUT);
}


void loop() {
  pauseState = digitalRead (pauseIn);
  thresholdState = digitalRead (thresholdIn);
  if (pauseState == HIGH)
  {
    if (thresholdState == HIGH)
    {
      digitalWrite(pauseOut, HIGH);
    }
  }
  else
  digitalWrite (pauseOut, LOW);
}
